
from machine import Pin, UART, I2C, SoftI2C
import onewire
import ds18x20
# from displays import Displays
import dht

import tm1637
from rotary_irq_esp import RotaryIRQ
import bme280_float as bme280

pin_i2c_scl = 22
pin_i2c_sda = 21

pin_meteo = 27
pin_meteo_out = 14
pin_led = 13
pin_led_rgb_r = None
pin_led_rgb_g = 0
pin_led_rgb_b = 2
pin_neopixel = 16

pin_ds18B20 = 15

pin_mq7 = 34

pin_tm1637_clk = 26
pin_tm1637_dio = 25

pin_rotary_clk = 32
pin_rotary_dt = 33

bme280_i2c_address = 0x76

display_sh1106 = 0
display_1602 = 1

class sensors_on:
    meteo = 1
    meteo_out = 1
    air = 1
    water = 1
    mq7 = 1
    bme280 = 1
class pinAssign:
    def __init__(self):
        pass

    def dsScan(self):
        ds_pin = Pin(pin_ds18B20)
        ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))
        roms = ds_sensor.scan()
        if len(roms) == 0 or ds_pin.value() == 0:
            print("No DS device !")
            return None, None
        else:
            print('DS devices found:', len(roms), roms)
            return ds_sensor, roms

    def displayScan(self, adr):
        #i2c = SoftI2C(scl=Pin(22), sda=Pin(21), freq=500000)

        if Pin(pin_i2c_scl).value() == 0:
            devices = []
        else:
            i2c = I2C(0, scl=Pin(pin_i2c_scl), sda=Pin(pin_i2c_sda), freq=400000)
            devices = i2c.scan()

        if len(devices) == 0:
            print("No i2c device !")
            return None
        else:
            print('i2c devices found:', len(devices), devices)
            # for i in range(len(devices)):
            #     print(devices[i])
            if adr in devices:
                # idx = devices.index(adr)
                return i2c
            else:
                return None

    def getDisplay(self, adr):
        dev = self.displayScan(adr)
        return dev

    def sensorMeteo1(self):
        pinn = Pin(pin_meteo)
        if pinn.value():
            return dht.DHT22(pinn)
        else:
            return None

    def sensorMeteoOut(self):
        pinn = Pin(pin_meteo_out)
        if pinn.value():
            return dht.DHT22(pinn)
        else:
            return None

    def sensorDS(self):
        dev, roms = self.dsScan()
        return dev, roms

    # def sensorMQ7(self):
    #     sensor = MQ7(pinData=pin_mq7)
    #     return sensor

    def getTM1637(self):
        dev = tm1637.TM1637(clk=Pin(pin_tm1637_clk), dio=Pin(pin_tm1637_dio))
        return dev

    def getRotary(self):
        r = RotaryIRQ(
            pin_num_clk=pin_rotary_clk,
            pin_num_dt=pin_rotary_dt,
            reverse=False,
            incr=1,
            range_mode=RotaryIRQ.RANGE_UNBOUNDED,
            pull_up=False,
            half_step=True,
        )
        return r

    def getBme280(self):
        dev = self.displayScan(bme280_i2c_address)
        if dev:
            return bme280.BME280(i2c=dev)
        else:
            return None

class pinLedMain:
    def __init__(self):
        self.led = Pin(pin_led, Pin.OUT)
        self.led_rgb_r = Pin(pin_led_rgb_r, Pin.OUT) if pin_led_rgb_r is not None else None
        self.led_rgb_g = Pin(pin_led_rgb_g, Pin.OUT) if pin_led_rgb_g is not None else None
        self.led_rgb_b = Pin(pin_led_rgb_b, Pin.OUT) if pin_led_rgb_b is not None else None
        self.led_neopixel = Pin(pin_neopixel, Pin.OUT) if pin_neopixel is not None else None

    def get(self):
        return self.led

    def get_red(self):
        return self.led_rgb_r

    def get_green(self):
        return self.led_rgb_g

    def get_blue(self):
        return self.led_rgb_b

    def get_neopixel(self):
        return self.led_neopixel