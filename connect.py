from wifi import wifi_connect_home, wifi_connect_phone, wifi_disconnect
# from sim800l2 import SIM800L
from SIM800L import Modem

UART_BAUD = 115200

# defaule use SIM800L_IP5306_VERSION_20190610
MODEM_POWER_PIN = 23
MODEM_RST = 5
MODEM_PWRKEY_PIN = 4
MODEM_TX = 26
MODEM_RX = 27

gprs_allow = False

class internet:
    def __init__(self):
        self.wifi_home = True
        self.wifi_phone = True
        self.gprs = False
        self.ready = False
        self.ip_address = ''
        self.wifi_alwaysOn = True
        self.fg_modemsim = False
        self.modem = None

    def start(self):
        print("Start internet...")
        if not self.ready:
            self.gprs = False
            # try wifi
            for i in range(2):
                if self.wifi_home:
                    self.ready, self.ip_address = wifi_connect_home()
                if not self.ready and self.wifi_phone:
                    self.ready, self.ip_address = wifi_connect_phone()
                if not self.ready:
                    self.wifi_home = not self.wifi_home
                    self.wifi_phone = not self.wifi_phone
                else:
                    break
            # try gprs
            if not self.ready and gprs_allow:
                if not self.fg_modemsim:
                    self.fg_modemsim = True
                    self.modem = Modem(MODEM_PWKEY_PIN=MODEM_PWRKEY_PIN, MODEM_RST_PIN=MODEM_RST,
                                       MODEM_POWER_ON_PIN=MODEM_POWER_PIN, MODEM_TX_PIN=MODEM_TX, MODEM_RX_PIN=MODEM_RX)
                    # self.modem = Modem(MODEM_TX_PIN=MODEM_TX, MODEM_RX_PIN=MODEM_RX)
                    print("modem initialize...")
                    self.modem.initialize()
                if self.modem:
                    ip_addr = self.modem.connect(apn='internet.tele2.ru')
                    print('\nModem IP address: "{}"'.format(ip_addr))
                    self.gprs = True
                    if not ip_addr is None:
                        self.ready = True
                        self.ip_address = ip_addr

        return self.ready

    def stop(self):
        self.ready = False
        print("Stop internet...")
        if self.gprs:
            print("Stop internet gprs...")
            self.modem.disconnect()
            print("Stoped internet gprs.")
            return
        if self.ready and not self.wifi_alwaysOn:
            # print("Stop internet wifi...")
            wifi_disconnect()
        self.gprs = False
        self.ready = False
        self.ip_address = ''


if __name__ == '__main__':
    import json
    from db_control import mqtt_work
    from time import sleep

    payload = {
        'device': 'sensors_kit1',  # Идентификатор устройства
        'message': '',  # ubinascii.a2b_base64(msg).decode('utf-8')
        'temperature': 10,
        'humidity': 10,
        'temperature_air': 10,
        'temperature_water': 10,
        'temperature_out': 10,
        'humidity_out': 10,
        'temperature_out_bme': 10,
        'humidity_out_bme': 10,
        'pressure': 10,
        'gas_co': 10
        # 'timestamp': int(time.time())  # Метка времени
    }
    dat = {'payload': payload, 'msg': "esp32/meteo"}
    data = json.dumps(payload)

    modem = Modem(MODEM_PWKEY_PIN=MODEM_PWRKEY_PIN, MODEM_RST_PIN=MODEM_RST, MODEM_POWER_ON_PIN=MODEM_POWER_PIN,
                  MODEM_TX_PIN=MODEM_TX, MODEM_RX_PIN=MODEM_RX)
    print("modem initialize...")
    modem.initialize()
    #     modem.execute_at_command({'setapn': {'string': 'AT+CGMR', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTT?', 'timeout': 3, 'end': 'OK'}})

    # #     modem.connect(apn='internet')
    #     print('\nModem IP address: "{}"'.format(modem.get_ip_addr()))
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSHUT', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPMUX=0', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CSTT="{}","",""'.format('internet'), 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIICR', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIFSR', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTART="TCP","kvv.hopto.org","1883"', 'timeout': 3, 'end': 'OK'}})

    #     modem.execute_at_command({'setapn': {'string': 'AT+CRESET', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CSQ', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CGATT?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CGATT?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=3,1,"CONTYPE","GPRS"', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=3,1,"APN","internet.tele2.ru"', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=3,1,"USER","tele2"', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=3,1,"PWD","tele2"', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=1,1', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=2,1', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CGACT=1,1', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CGPADDR=1', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CEREG?', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CGERROR?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+HTTPINIT', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+HTTPPARA=\"CID\",1', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTART="TCP","kvv.hopto.org","1883"', 'timeout': 3, 'end': 'OK'}})
    #

    #     modem.execute_at_command({'setapn': {'string': 'AT+CPIN?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CSQ', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CREG?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CGATT?', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPMODE=0', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPMUX=0', 'timeout': 3, 'end': 'OK'}})
    #
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPCLOSE', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'ATV1', 'timeout': 10, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'AT+CMEE=2', 'timeout': 10, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'AT+CIPSHUT', 'timeout': 10, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CSCLK=0', 'timeout': 10, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPRXGET=0', 'timeout': 10, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+IFC=2,2', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPMODE=1', 'timeout': 3, 'end': 'OK'}})

    # #

    #     modem.execute_at_command({'setapn': {'string': 'AT+CGATT=1', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=3,1,"CONTYPE","GPRS"', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command(
        {'setapn': {'string': 'AT+SAPBR=3,1,"APN","internet.tele2.ru"', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'AT+CGATT=1,1', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=1,1', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command({'setapn': {'string': 'AT+SAPBR=2,1', 'timeout': 3, 'end': 'OK'}})

    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CSTT="internet.tele2.ru"', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIICR', 'timeout': 30, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIFSR', 'timeout': 3, 'end': 'OK'}})
    modem.execute_at_command(
        {'setapn': {'string': 'AT+CIPSTART="TCP","kvv.hopto.org",1883', 'timeout': 7, 'end': 'CONNECT'}})

    modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 15, 'end': 'CONNECT OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'ATO', 'timeout': 3, 'end': 'OK'}})

    topic = "esp32/meteo"

    mqtt_packet = bytearray([
        0x30,  # PUBLISH packet type
        len(topic) + len(data) + 2,  # Remaining length
        0x00, len(topic),  # Topic length
    ])
    mqtt_packet.extend(topic.encode())
    mqtt_packet.extend(data.encode())
    #
    modem.execute_at_command(
        {'setapn': {'string': 'AT+CIPSEND={}'.format(len(mqtt_packet)), 'timeout': 1, 'end': '> '}})
    #     send_at('AT+CIPSEND={}'.format(len(mqtt_packet)))
    sleep(1)
    modem.uart.write(mqtt_packet)
    i = 0
    sleep(1)
    modem.uart.write(chr(26))
    while True:

        line = modem.uart.readline()
        if not line:
            sleep(2)
            i += 1
            if i > 10:
                print('not line')
                break
        print(line)
    modem.execute_at_command({'setapn': {'string': 'AT+CIPCLOSE', 'timeout': 5, 'end': 'OK'}})

    modem.execute_at_command({'setapn': {'string': 'AT+CIPSTATUS', 'timeout': 15, 'end': 'CONNECT OK'}})
    # #
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSEND?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPQSEND?', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSEND=100', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPMODE=1', 'timeout': 0, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSEND', 'timeout': 0, 'end': 'OK'}})
    #
    #     modem.execute_at_command({'setapn': {'string': 'AT+IFC=1,1', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSEND', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSEND', 'timeout': 3, 'end': 'OK'}})
    # #     modem.execute_at_command({'setapn': {'string': 'AT+CIPSEND', 'timeout': 3, 'end': 'OK'}})

    #     modem.execute_at_command({'setapn': {'string': 'AT+SCKT=1,"kvv.hopto.org",1883', 'timeout': 3, 'end': 'OK'}})
    #     topic ="esp32/meteo"
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTTSTART', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTTACCQ=0,"esp32tcall_gazag-s123"', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTTCONNECT=0,"tcp:\\kvv.hopto.org:1883",60,1', 'timeout': 3, 'end': 'OK'}})
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTTTOPIC=0,{}'.format(len(topic)), 'timeout': 3, 'end': 'OK'}})
    #     modem.uart.write(topic)
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTTPAYLOAD=0,{}'.format(len(payload)), 'timeout': 3, 'end': 'OK'}})
    #     modem.uart.write(payload)
    #     modem.execute_at_command({'setapn': {'string': 'AT+CMQTTPUB=0,1,60', 'timeout': 3, 'end': 'OK'}})

    #     print('Send DATA...')
    # #     modem.uart.write(data.encode('utf-8'))
    #     modem.uart.write("{}\r\n".format(data).encode('utf-8'))
    #     i = 0
    #     while True:
    #
    #         line = modem.uart.readline()
    #         if not line:
    #             sleep(2)
    #             i += 1
    #             if i > 3:
    #                 print('not line')
    #                 break
    #         print(line)
    mqtt_work().sendMeteo('', 15, 15, 15, 15,
                          15, 15, 15, 15,
                          15, 15)

#     print('\nNow running demo http GET...')
#     url = 'http://checkip.dyn.com/'
#     response = modem.http_request(url, 'GET')
#     print('Response status code:', response.status_code)
#     print('Response content:', response.content)
#
#     print('\nNow running demo http GET DONE')
#     # Example POST
#     print('Now running demo https POST...')
#     url  = 'https://postman-echo.com/post'
#     data = json.dumps({'myparameter': 42})
#     response = modem.http_request(url, 'POST', data, 'application/json')
#     print('Response status code:', response.status_code)
#     print('Response content:', response.content)

# Disconnect Modem
#     modem.disconnect()


#     output = modem.execute_at_command('level')
#     print('output ' + str(output))
#     output = modem.execute_at_command('checkreg')
#     print('output checkreg ' + str(output))
#     output = modem.execute_at_command('getbear')
#     print('output getbear ' + str(output))
#
#
#     modem.disconnect()
#
#     modem.connect(apn='internet.tele2.ru')
#     modem.disconnect()