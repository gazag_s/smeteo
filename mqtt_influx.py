import paho.mqtt.client as mqtt
import requests
import json
from influxdb_client import InfluxDBClient, Point, WriteOptions
from datetime import datetime

# Настройки MQTT
MQTT_BROKER = "localhost"  # Адрес MQTT брокера
MQTT_PORT = 1883                  # Порт MQTT
MQTT_TOPIC = "esp32/meteo"         # Тема MQTT
MQTT_CLIENT_ID = "esp32tcall_gazag-s123"    # ID клиента MQTT

# Настройки InfluxDB
INFLUXDB_URL = "http://localhost:8086"  # URL InfluxDB
INFLUXDB_TOKEN = "knQsIV4xu_BQv2oAJWgz7pqV_Lb-A6Uh_iL-8GU_0PSHKK_JsYr_qBhQPS9s3L3YHCZlSfII_X-UIvypXqDk_A=="          # Токен для доступа к InfluxDB
INFLUXDB_ORG = "Dom"              # Организация в InfluxDB
INFLUXDB_BUCKET = "test"        # Бакет (база данных) InfluxDB

# Создаем клиент для InfluxDB
influxdb_client = InfluxDBClient(url=INFLUXDB_URL, token=INFLUXDB_TOKEN)
# # Настройки InfluxDB
# INFLUXDB_URL = 'http://localhost:8086/write?db=test'
# MEASUREMENT = 'meteo'
# TAG1 = 'device'
# 
# # Функция для записи в InfluxDB
# def write_to_influxdb(temperature,humidity,temperature_air):
#     payload = f'{MEASUREMENT},{TAG1}=ESP32_tcall temperature={temperature},humidity={humidity},temperature_air={temperature_air}'
#     headers = {'Content-Type': 'application/x-www-form-urlencoded'}
#     
#     try:
#         response = requests.post(INFLUXDB_URL, data=payload, headers=headers)
#         if response.status_code == 204:
#             print('Данные успешно записаны в InfluxDB')
#         else:
#             print(f'Ошибка записи в InfluxDB: {response.status_code}, {response.text}')
#     except Exception as e:
#         print(f'Ошибка подключения к InfluxDB: {e}')

def on_connect(client, userdata, flags, rc):
#     print(f"Connected to MQTT broker with result code {rc}")
    # Подписка на топик
    client.subscribe(MQTT_TOPIC)
    
# Функция обработки сообщений MQTT
def on_message(client, userdata, msg):
    tm = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    try:
        print(f' {tm}: Получено сообщение: {msg.payload.decode()} топик {msg.topic}')
#         # Преобразование сообщения в JSON (предполагается, что данные в JSON-формате)
# 
#         data = json.loads(msg.payload)
#         
#         # Извлекаем температуру из сообщения и записываем в InfluxDB
#         temperature = data['temperature']
#         humidity = data['humidity']
#         temperature_air = data['temperature_air']
#         write_to_influxdb(temperature, humidity, temperature_air)
#

        data = json.loads(msg.payload.decode())
            
        # Создание точки данных для InfluxDB
#         point = Point("MEAS") \
#             .tag("device", msg.topic)

        point = Point("MEAS") \
            .tag("device", data["device"])
    
        for k,v in data.items():
            
            if v:
                if isinstance(v, int):
                    v = float(v)
                point.field(k, v)
                
#             .field("humidity", data["humidity"]) \
#             .field("temperature_air", data["temperature_air"]) \
#             .field("temperature_water", data["temperature_water"]) \
#             .field("temperature_out", data["temperature_out"]) \
#             .field("humidity_out", data["humidity_out"]) \
#             .field("temperature_out_bme", data["temperature_out_bme"]) \
#             .field("humidity_out_bme", data["humidity_out_bme"]) \
#             .field("pressure", data["pressure"]) \
#             .field("gas_co", data["gas_co"])   # Здесь предполагается, что в сообщении есть поле "value"

        # Запись данных в InfluxDB
        write_api = influxdb_client.write_api(write_options=WriteOptions(batch_size=500))
        write_api.write(INFLUXDB_BUCKET, INFLUXDB_ORG, point)
        print("Data written to InfluxDB")

    except Exception as e:
        print(f"Error processing message: {e}")
  
# Настройка клиента MQTT
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

# client.callback_api_version = 2

# Подключение к MQTT брокеру
client.connect(MQTT_BROKER, MQTT_PORT)

# Запуск цикла обработки сообщений
client.loop_forever()
        
#         # Настройка клиента MQTT
# mqtt_client = mqtt.Client()
# mqtt_client.on_message = on_message
# 
# # Подключение к MQTT брокеру
# mqtt_client.connect(MQTT_BROKER, MQTT_PORT, 60)
# mqtt_client.subscribe(MQTT_TOPIC)
# 
# # Запуск клиента MQTT
# mqtt_client.loop_forever()

#  Автозапуск скрипта в systemd
#  после измерения перезапустить systemd:
#  sudo systemctl stop myscript.service
#  sudo systemctrl restart myscript.service
