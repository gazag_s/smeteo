import usocket as socket
import gc
import _thread
gc.collect()

temp_web = -100
him_web = -100
class webpage1:
    def __init__(self):
        self.temp = -100
        self.him = -100

    def get_html(self, temp = None, hum = None):
        self.html = """<!DOCTYPE HTML><html><head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <style> html { font-family: Arial; display: inline-block; margin: 0px auto; text-align: center; }
          h2 { font-size: 3.0rem; } p { font-size: 3.0rem; } .units { font-size: 1.2rem; } 
          .ds-labels{ font-size: 1.5rem; vertical-align:middle; padding-bottom: 15px; }
        </style></head><body><h2>ESP32 Myagi 24b-204 WebServer</h2>
        <p><i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
          <span class="ds-labels">Temperature</span>
          <span id="temperature">""" + str(temp) + """</span>
          <sup class="units">&deg;C</sup>
        </p>
          <p><i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
          <span class="ds-labels">Humidity</span>
          <span id="humidity">""" + str(hum) + """</span>
          <sup class="units">&deg;%</sup>
        </p></body></html>"""
        return self.html
 
 
    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(('', 80))
        self.sock.listen(5)
        _thread.start_new_thread(self.webpage1_thread, ())

    def update(self, temp=-100, him=-100):
        global temp_web, him_web
        temp_web = temp
        him_web = him
        self.temp = temp
        self.him = him

    def webpage1_thread(self):
        global temp_web, him_web
        while True:
            try:
                if gc.mem_free() < 102000:
                  gc.collect()
                conn, addr = self.sock.accept()
                conn.settimeout(3.0)
                print('Got a connection from %s' % str(addr))
                request = conn.recv(1024)
                conn.settimeout(None)
                request = str(request)
                print('Content = %s' % request)
                response = self.get_html(temp_web, him_web)
                conn.send('HTTP/1.1 200 OK\n')
                conn.send('Content-Type: text/html\n')
                conn.send('Connection: close\n\n')
                conn.sendall(response)
                conn.close()
            except OSError as e:
                conn.close()
                print('Connection closed')
                break
