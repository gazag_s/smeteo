# wifi connect
import time
import network # Импорт библиотеки

time_for_connect = 5 # sec

def wifi_connect_home():
    sta_if = network.WLAN(network.STA_IF) # Режим приёмника
    sta_if.active(True)
    sta_if.connect('gsa', '70000000')

    print('wifi home connecting...')
    time_wait = 0
    time_one = 0.2
    while sta_if.isconnected() == False or time_wait < time_for_connect:
        time.sleep(time_one)
        time_wait += time_one
    if sta_if.isconnected():
        print('Connection home successful')
        ip = sta_if.ifconfig()[0]
        print(ip)
        return True, ip
    else:
        print('Connection home failed')
        return False, ''

def wifi_connect_phone():
    sta_if = network.WLAN(network.STA_IF) # Режим приёмника
    sta_if.active(True)
    sta_if.connect('Redmi Note 11', '70000000')
    print('wifi phone connecting...')
    time_wait = 0
    time_one = 0.2
    while sta_if.isconnected() == False or time_wait < time_for_connect:
        time.sleep(time_one)
        time_wait += time_one
    if sta_if.isconnected():
        print('Connection phone successful')
        ip = sta_if.ifconfig()[0]
        print(ip)
        return True, ip
    else:
        print('Connection phone failed')
        return False, ''

def wifi_disconnect():
    sta_if = network.WLAN(network.STA_IF)
    sta_if.disconnect()