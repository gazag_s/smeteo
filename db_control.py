
# from pymysql import connect
import time
import network
from umqtt.simple import MQTTClient
import json
import ubinascii

# Настройки MQTT
# MQTT_BROKER = "192.168.1.123"  # Адрес вашего MQTT-брокера
MQTT_BROKER = "kvv.hopto.org"  # Адрес вашего MQTT-брокера
MQTT_PORT = 1883  # Порт MQTT (обычно 1883)
MQTT_TOPIC = 'esp32/meteo'  # Тема, в которую будем публиковать данные

class mqtt_work:
    # Функция для подключения к MQTT-брокеру
    def connect_mqtt(self):
        client = MQTTClient('esp32tcall_gazag-s123', MQTT_BROKER, port=MQTT_PORT)
        client.connect()
        print('Подключено к MQTT брокеру')
        return client

    # Функция для отправки данных в MQTT
    def sendMeteo(self, msg, temperature, humidity, temperature_air, temperature_water,
                            temperature_out, humidity_out, temperature_out_bme, humidity_out_bme,
                            pressure, gas_co):
        try:
            # Формируем данные в формате JSON
            payload = {
                'device': 'sensors_kit1',  # Идентификатор устройства
                'message': ubinascii.b2a_base64(msg.encode('utf-8')).strip(),   # ubinascii.a2b_base64(msg).decode('utf-8')
                'temperature': temperature,
                'humidity': humidity,
                'temperature_air': temperature_air,
                'temperature_water': temperature_water,
                'temperature_out': temperature_out,
                'humidity_out': humidity_out,
                'temperature_out_bme': temperature_out_bme,
                'humidity_out_bme': humidity_out_bme,
                'pressure': pressure,
                'gas_co': gas_co
                # 'timestamp': int(time.time())  # Метка времени
            }
            client = self.connect_mqtt()
            # Публикуем данные в заданную тему
            client.publish(MQTT_TOPIC, json.dumps(payload))
            client.disconnect()
            print('Данные отправлены в MQTT:', payload)
            return 0
        except:
            print('Ошибка отправки данных в MQTT!')
            return 1

class db_par:

    host = "192.168.1.123"
    port = "3306"
    user = "pi"
    pw = "ty45"
    name = "kvvdb"

class db_tables:
    class meteo:
        _name = 'Meteo'
        # --[
        temperature1 = 'DHT22_temperature'
        humidity1 = 'DHT22_humidity'
        temperatureAir = 'DS18B20_temperature'
        # ]--

class db_connect:

    def __init__(self):
        self.db_par = db_par()


    def update_connect_par(self, host, port, user, pw, name):
        self.db_par.host = host
        self.db_par.port = port
        self.db_par.user = user
        self.db_par.pw = pw
        self.db_par.name = name

    def connect(self):
        db_con = ''
        error = ''
        try:
            db_con = connect(host=self.db_par.host, port=self.db_par.port, user=self.db_par.user,
                                            password=self.db_par.pw, database=self.db_par.name)
        except connect.Error as e:
            error = e
        return db_con, error

    # def get_id_by_value(self, tab, data):
    #     # forming a querty and getting the ID field by the value of some specified field
    #     sql_query = ""
    #     values = []
    #     i = 0
    #     for k, v in data.items():
    #         if v:
    #             if i == 0:
    #                 # sql_query = self.sql_query_select_id_str % (tab, k, "'%s'" % v)
    #
    #                 sql_query = self.sql_query_select_id_str % (tab, k, '%s')
    #                 values.append(v)
    #             else:
    #                 # sql_query += " AND %s ='%s'" % (k, v)
    #                 sql_query += " AND %s =" % k + "%s"
    #                 values.append(v)
    #             i += 1
    #     # id = self.select_id_query(sql_query)
    #     id = self.select_id_value(sql_query, values)
    #     return id
    #
    # def get_value_by_id(self, tab, data_name, id):
    #     # forming a querty and getting the value field by the field name and ID value
    #     # id = list(id_dct.values())[0]
    #     sql_query = self.sql_query_select_data % (data_name, tab, 'id', id)
    #     ans = self.select_id_query(sql_query)
    #     return ans

    # def select_id_value(self, sql_Query, value):
    #     # getting the ID field by the value of some specified field
    #     db_con = ''
    #     id = None
    #     db_con, error = self.connect()
    #     if db_con and sql_Query:
    #         cursor = db_con.cursor(prepared=True, dictionary=True)
    #
    #         cursor.execute(sql_Query, value)
    #         id = cursor.fetchone()
    #         # try:
    #         #     cursor.execute(sql_Query, [value])
    #         #     id = cursor.fetchone()
    #         # except:
    #         #     print('not found %s' % value)
    #     return id
    #
    # def select_id_query(self, sql_Query):
    #     # obtaining an ID based on a generated query
    #     db_con = ''
    #     id = None
    #     db_con, error = self.connect()
    #     if db_con and sql_Query:
    #         cursor = db_con.cursor(dictionary=True)
    #
    #         cursor.execute(sql_Query)
    #         id = cursor.fetchone()
    #         # try:
    #         #     cursor.execute(sql_Query)
    #         #     id = cursor.fetchone()
    #         # except:
    #         #     print('not found')
    #     return id

    def insert_tab(self, tab, data_dct):
        sql_query, values = self.form_insert_into_querty(tab, data_dct)
        id = self.insert_into(sql_query, values)
        return id

    def update_tab(self, tab, data_dct, id_val):
        # id_val = list(id_dct.values())[0]
        sql_query, values = self.form_update_tab_querty(tab, data_dct, id_val)
        id = self.insert_into(sql_query, values, True)
        return id

    def form_insert_into_querty(self, table, dct):
        sql = r'INSERT INTO'
        val = []

        if dct:
            val_type = ''
            sql += ' %s' % table
            sql += ' ('
            for k, v in dct.items():
                sql += k + ', '
                val_type += '?,'
                val.append(v)
            if val_type:
                sql = sql[:-2] + ') VALUES (' + val_type[:-1] + ')'

        # for k,v in dct:
        #     sql += v + ', '
        # sql = sql[:-1] + ')'

        return sql, val

    def form_update_tab_querty(self, table, dct, id_val):
        sql = r'UPDATE'
        sql += ' %s' % table
        sql += ' SET '
        val = []

        if dct:
            for k,v in dct.items():
                sql += k + " = (?), "
                val.append(v)
                # if isinstance(v, str):
                #     sql += k + " = '%s'," % v
                # else:
                #     sql += k + " = %s," % v

        sql = sql[:-2] + ' WHERE id = %s' % id_val

        return sql, val

    def insert_into(self, querty, val, fg=False):
        db_con = ''
        id = None
        db_con, error = self.connect()
        if db_con:
            with db_con.cursor(prepared=True) as cursor:
                cursor.execute(querty, val)
                # if fg: cursor.execute('select LAST_INSERT_ID()')
                id = cursor.lastrowid
                db_con.commit()
        db_con.close()
        return id

class db_work:

    def __init__(self):
        self.con = db_connect()
        self.tab = db_tables()

    def sendMeteo(self, temp1, hmdt1, tempA):
        data = {}
        data.update({self.tab.meteo.temperature1: temp1})
        data.update({self.tab.meteo.humidity1: hmdt1})
        data.update({self.tab.meteo.temperatureAir: tempA})

        self.con.insert_tab(self.tab.meteo._name, data)
