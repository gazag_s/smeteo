import _thread
from time import time, sleep, sleep_ms, localtime

from machine import RTC
import ntptime
import utime
import math
# NTP_SERVER = '0.de.pool.ntp.org'
NTP_SERVER = 'time.google.com'

ntptime.host = NTP_SERVER
UTC_OFFSET = 4  # Samara is UTC+4:00

tempAir_lim0 = -3
tempAir_lim1 = 50
tempAir_lim2 = 60
tempWater_lim0 = 3
tempWater_lim1 = 60
tempOut_lim0 = -20
tempOut_lim1 = 30
co_lim = 150

time_upd_ntc_period = 7200
time_upd_max = 3600
time_upd_meteo = 2
time_upd_ds = 30
time_upd_bme = 1
num_ds_air = 0
num_ds_water = 1

messages = ['На улице холодает',
            '',
            '',
            'Парная нагрелась',
            'Вода замерзает',
            'Вода нагрелась',
            'ОБНАРЕЖЕН УГАРНЫЙ ГАЗ!']


# Мьютекс для синхронизации
mutex1 = _thread.allocate_lock()
mutex2 = _thread.allocate_lock()
mutex3 = _thread.allocate_lock()
mutex4 = _thread.allocate_lock()
class Model:
    timeUpdateData = [3600, 1200, 800, 400]
    wrong_val = -99
    def __init__(self):
        print("Model init...")
        self.rtc = RTC()

        self.sensorMeteo = None
        self.devTAir = [None, None]
        self.devDS = [None, None]
        self.updateData = ''
        self.mq7 = None
        self.tm1637 = None
        self.rotary = None
        self.bme280 = None

        self.sendStatus = 0
        self.sumSendStatus = 0
        self.rotary_sign = 0

        self.time_last_sensorMeteo = time()
        self.time_last_sensorDS = time()
        self.time_last_sensorBME = time()
        self.time_last_sensorMeteo_2 = time()
        self.time_last_sensorDS_2 = time()
        self.time_upd_ntc = time() - time_upd_ntc_period

        self.timer_now = 0
        self.timer_step = 5*60                 # sec
        self.timer_time = self.timer_step     # sec
        self.timer_fg = 0

        self.co_value = self.wrong_val

        self.fun_tm1637_list = [self.writeTime, self.writeTemperature, self.writeTemperatureAir, self.writeSendStatus,
                                self.startTimer]
        self.fun_tm1637 = self.writeTime

    def update_functions(self, func_update_data, timeUpdateData):
        self.updateData = func_update_data
        self.timeUpdateData = timeUpdateData

    def setWrongVal(self, val):
        self.wrong_val = val

# %%----------------------------------------------------%%
    def setMQ7(self, sensor):
        try:
            self.mq7 = sensor
            _thread.start_new_thread(self.WorkCO, ())
        # print(self.mq7._ro)
        # print(self.mq7.getRoInCleanAir)
        except:
            self.mq7 = None

    def getCO(self):
        return self.co_value

    def WorkCO(self):
        if self.mq7:
            self.co_value = -1
            self.mq7.calibrate()
            self.co_value = 0
            print('WorkCO')
            while True:
                try:
                    self.co_value = self.mq7.readCarbonMonoxide()
                except:
                    self.co_value = self.wrong_val
                    # break
            # else:
            #     break
                sleep(20)

# %%----------------------------------------------------%%
    def setTM1637(self, tm1637):
        self.tm1637 = tm1637
        if self.tm1637:
            self.tm1637.write([127, 255, 127, 127])
            self.tm1637.brightness(2)

    def startWorkTM1637(self):
        _thread.start_new_thread(self.WorkTM1637, ())

    def WorkTM1637(self):
        #print('WorkTM1637')
        while True:
            if self.tm1637:
                try:
                    self.fun_tm1637()
                    sleep(0.1)
                except:
                    break
            else:
                break
        try:
            self.tm1637.show('erro')
        except:
            print("Wrong WorkTM1637!")

    def writeTime(self):
        #(year, month, day, wday, hrs, mins, secs, subsecs) = self.rtc.datetime()
        # (year, month, day, hrs, mins, secs, wday, yday, subsecs) = localtime()
        (year, month, day, hrs, mins, secs, wday, yday) = localtime()
        self.tm1637.numbers(hrs, mins)
        sleep(0.5)

    def writeTemperature(self):
        # Проверка времени обновления датчика для предотвращения слишком частого опроса/считывания,
        # также для своевременного обновления экрана сразу после измерения
        if (abs(self.time_last_sensorMeteo_2 - self.time_last_sensorMeteo) > time_upd_meteo):
            self.time_last_sensorMeteo_2 = self.time_last_sensorMeteo
            if self.sensorMeteo:
                self.tm1637.temperature(int(self.sensorMeteo.temperature()))
            else:
                self.tm1637.show('None')
        sleep(0.2)

    def writeTemperature2(self):
        self.tm1637.temperature(int(-99))
        sleep(0.2)

    def writeTemperatureAir(self):
        if (abs(self.time_last_sensorDS_2 - self.time_last_sensorDS) > time_upd_meteo):
            self.time_last_sensorDS_2 = self.time_last_sensorDS
            tempAir, tempWater = self.getDStemp()
            if tempAir:
                self.tm1637.temperature(int(tempAir))
        sleep(0.2)

    def writeSendStatus(self):
        self.tm1637.show('    ')
        txt = str(self.sumSendStatus) + str(self.sendStatus)
        self.tm1637.show(txt[-4:])
        sleep(0.5)

    def startTimer(self):
        if self.timer_fg == 0:
            if (time() - self.timer_now) < 10:
                self.timer_time += self.timer_step*self.rotary_sign
            if self.timer_time/60 > 99 or self.timer_time <= 0:
                self.timer_time = self.timer_step
            self.timer_now = time()

            # self.timer_now = math. self.timer_now / self.timer_step
            # if (time() - self.timer_now) > 10:
            #     self.timer_now = self.timer_now / self.timer_step

        self.timer_fg = 1
        self.tm1637.show('    ')
        t = self.timer_time - (time() - self.timer_now)
        if t <= 0:
            self.timer_now = time()
        min = math.floor(t/60)
        sec = t - min*60
        self.tm1637.numbers(min, sec)
        sleep(0.25)



# %%----------------------------------------------------%%
    def setMeteo(self, meteo, meteoOut):
        self.sensorMeteo = meteo
        self.sensorMeteoOut = meteoOut
    def startMeteo(self):
        _thread.start_new_thread(self.workMeteo, ())

    def workMeteo(self):
        #print('workMeteo')
        while True:
            if self.sensorMeteo:
                try:
                    self.sensorMeteo.measure()
                    sleep(20)
                except:
                    break
            else:
                break

    def getMeteo(self):
        tmp1 = self.wrong_val
        hum1 = self.wrong_val
        tmpOut = self.wrong_val
        humOut = self.wrong_val
        mutex1.acquire()  # Захват мьютекса
        try:
            diff_time = (time() - self.time_last_sensorMeteo)
            if self.sensorMeteo:
                if (diff_time > time_upd_meteo) or (diff_time < 0):
                    self.sensorMeteo.measure()
                    self.time_last_sensorMeteo = time()
                tmp1 = self.sensorMeteo.temperature()
                hum1 = self.sensorMeteo.humidity()
        except:
            self.time_last_sensorMeteo = time()
            print("Wrong getMeteo!")
        try:
            diff_time = (time() - self.time_last_sensorMeteo)
            if self.sensorMeteoOut:
                if (diff_time < 2) or (diff_time > time_upd_max):
                    self.sensorMeteoOut.measure()
                    self.time_last_sensorMeteo = time()
                tmpOut = self.sensorMeteoOut.temperature()
                humOut = self.sensorMeteoOut.humidity()
        except:
            self.time_last_sensorMeteo = time()
            print("Wrong getMeteoOut!")
        mutex1.release()  # Освобождение мьютекса
        return tmp1, hum1, tmpOut, humOut

# %%----------------------------------------------------%%

    def setSensorTAir(self, dev, roms):
        self.devTAir = [dev, roms]
    def getTair(self):
        val = self.wrong_val
        mutex3.acquire()  # Захват мьютекса
        try:
            if not self.devTAir[0] is None:
                self.devTAir[0].convert_temp()
                sleep_ms(150)
                for rom in self.devTAir[1]:
                    val = self.devTAir[0].read_temp(rom)
                    if val:
                        break
        except:
            print("Wrong getTair!")
        mutex3.release()  # Освобождение мьютекса
        return val

# %%----------------------------------------------------%%
    def setSensorDS(self, dev, roms):
        self.devDs = [dev, roms]

    def getDStemp(self):
        air = self.wrong_val
        water = self.wrong_val
        mutex4.acquire()  # Захват мьютекса
        try:
            diff_time = (time() - self.time_last_sensorDS)
            if not self.devDs[0] is None:
                if (diff_time > time_upd_ds) or (diff_time < 0):
                    self.devDs[0].convert_temp()
                    self.time_last_sensorDS = time()
                    sleep_ms(750)
                i = 0
                for rom in self.devDs[1]:
                    val = self.devDs[0].read_temp(rom)
                    if i == num_ds_air:
                        air = val
                    elif i == num_ds_water:
                        water = val
                    i += 1
        except:
            print("Wrong getDS!")
        mutex4.release()  # Освобождение мьютекса
        return air, water

# %%----Time------------------------------------------------%%
    def updateRTCTime(self):
        i = 1
        while i > 0:
            try:
                if abs(time() - self.time_upd_ntc) > time_upd_ntc_period:
                    print("Internet update RTC")
                    ntptime.settime()
                    # tm = utime.localtime(utime.mktime(utime.localtime()) + UTC_OFFSET*3600)
                    # print(tm)
                    sec = ntptime.time()
                    sec = int(sec + UTC_OFFSET * 60 * 60)
                    (year, month, day, hrs, mins, secs, wday, yday) = localtime(sec)
                    self.rtc.datetime((year, month, day, wday, hrs, mins, secs, 0))
                    # (year, month, day, wday, hrs, mins, secs, subsecs) = self.rtc.datetime()
                    (year, month, day, hrs, mins, secs, wday, yday) = localtime()
                    print(f"{year}-{month}-{day} {hrs}:{mins}:{secs}")
                    self.time_upd_ntc = time()
                i = -1
            except:
                i -= 1

# %%----Rotary------------------------------------------------%%
    def setRotary(self, rotary):
        self.rotary = rotary

    def updateRotaryFunc(self, num):
        if self.rotary:
            self.rotary_sign = num
            idx = self.fun_tm1637_list.index(self.fun_tm1637)
            if (idx+num) >= len(self.fun_tm1637_list):
                idx = 0
                num = 0
            self.fun_tm1637 = self.fun_tm1637_list[idx+num]
            self.time_last_sensorMeteo_2 = 0
            self.time_last_sensorDS_2 = 0
            self.timer_fg = 0

    def startworkRotary(self):
        _thread.start_new_thread(self.workRotary, ())

    def workRotary(self):
        if self.rotary:
            val_old = self.rotary.value()
            while True:
                val_new = self.rotary.value()

                if val_old != val_new:
                    self.updateRotaryFunc((val_new>val_old)-(val_new<val_old))
                    val_old = val_new

                sleep_ms(100)

# %%----BME280------------------------------------------------%%
    def setBme280(self, dev):
        self.bme280 = dev

    def getMeteoBME(self):
        temp = self.wrong_val
        hum = self.wrong_val
        pres = self.wrong_val
        mutex2.acquire()  # Захват мьютекса
        try:
            diff_time = (time() - self.time_last_sensorBME)
            if self.bme280:
                if (diff_time < time_upd_bme):
                    sleep(0.5)
                t, p, h = self.bme280.read_compensated_data()
                self.time_last_sensorBME = time()
                temp = t
                hum = h
                pres = p/133.322
                # temp = t/100.0
                # hum = h/1024.0
                # pres = p/25600.0
        except:
            print("Wrong getMeteoBME!")
        mutex2.release()  #  Освобождение мьютекса
        return temp, hum, pres


# %%----MAIN------------------------------------------------%%
    def startCalcMeteo(self):
        _thread.start_new_thread(self.calcMeteo, ())

    def calcMeteo(self):
        sleep(30)
        n = 0
        fg = None
        fg_last = None
        msg = ''
        fg_update = False
        timeUpdateData = self.timeUpdateData[0]
        timeout = timeUpdateData
        start_time = time()
        while True:
            n += 1
            temp1, hmdt1, tempOut, hmdtOut = self.getMeteo()
            # tempAir = self.getTair()
            tempAir, tempWater = self.getDStemp()
            co = self.getCO()
            if 1: #temp1 and hmdt1 and tempAir:
                if (tempOut < tempOut_lim0) and tempOut != self.wrong_val and fg != 0:
                    fg = 0
                if (tempOut > tempOut_lim1) and tempOut != self.wrong_val and fg != 1:
                    fg = 1
                if (tempAir < tempAir_lim0) and tempAir != self.wrong_val and fg != 2:
                    fg = 2
                if (tempAir > tempAir_lim1) and (tempAir <= tempAir_lim2) and tempAir != self.wrong_val and fg != 3:
                    fg = 3
                    timeUpdateData = self.timeUpdateData[1]
                elif (tempAir < tempAir_lim1) and fg == 3:
                    timeUpdateData = self.timeUpdateData[0]
                if (tempAir > tempAir_lim2) and tempAir != self.wrong_val:
                    fg = 4
                    timeUpdateData = self.timeUpdateData[3]
                # elif (tempAir < tempAir_lim2) and fg == 4:
                #     timeUpdateData = self.timeUpdateData[1]
                if (tempWater < tempWater_lim0) and tempWater != self.wrong_val and fg != 5:
                    fg = 5
                if (tempWater > tempWater_lim1) and tempWater != self.wrong_val and fg != 6:
                    fg = 6
                    timeUpdateData = self.timeUpdateData[2]
                elif (tempWater < tempWater_lim1) and fg == 6:
                    timeUpdateData = self.timeUpdateData[0]
                if (co > co_lim) and co != self.wrong_val and fg != 7:
                    fg = 7
                    timeUpdateData = self.timeUpdateData[3]
                elif (co < co_lim) and fg == 7:
                    timeUpdateData = self.timeUpdateData[0]

                if fg != fg_last:
                    fg_last = fg
                    timeout = -1
                    msg = ''
                    if fg == 0:
                        msg = messages[0]
                    elif fg == 4:
                        msg = messages[4]
                    elif fg == 5:
                        msg = messages[5]
                    elif fg == 6:
                        msg = messages[6]
                    elif fg == 7:
                        msg = messages[7]

                if (timeout < 0) or (timeout > max(self.timeUpdateData)):
                    fg_update = True
                    timeout = timeUpdateData
                    start_time = time()

                if fg_update:
                    print('n: ' + str(n))
                    fg_update = False
                    if self.updateData:
                        self.updateData(msg)
                    msg = ''
                else:
                    sleep(30)
            sleep(30)
            timeout -= time() - start_time
            start_time = time()
            print(timeout)
    # def addObserver(self, inObserver):
    #     self.observers.append(inObserver)
    #
    # def updateObserver(self, temp1, hmdt1, tempAir):
    #     pass

# %%----OTHERS------------------------------------------------%%
    def set_sendStatus(self, val):
        self.sendStatus = val
        if self.sumSendStatus < 999:
            self.sumSendStatus += 1 - val

if __name__ == "__main__":
    from controller import Controller

    model = Model()
    ctrl = Controller(model)
    ctrl.settingModel()
    ctrl.startCalcMeteo()
    ctrl.startWorkTM1637()
    ctrl.startworkRotary()

    # Model().startMeteo()