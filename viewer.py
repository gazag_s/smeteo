from time import time, sleep
from displays import Displays
# from db_control import db_work
from db_control import mqtt_work
import math
import _thread

t_base_name = 'i:'
t_out_name = 'o:'
h_base_name = 'hb: '
t_air_name = 'a'
t_water_name = 'w'
th_bme_name = 'O:'
p_bme_name = 'P:'
co_name = 'co:'


class Viewer:
    global thread_internet
    thread_internet = False

    timeUpdateView = 10
    timeUpdateData = [360, 1200, 800, 400]

    wrong_val = -99

    def __init__(self, inCtrl):
        self.ctrl = inCtrl
        self.ctrl.setWrongVal(self.wrong_val)
        # self.dbw = db_work()
        self.mqtt = mqtt_work()
        self.display = Displays()
        self.display.display_1106()
        self.display.display_1602()
        # self.ctrl.ledMainBlink()
        self.ip_address = ''
        self.fg1_ip_address = True
        self.fg_updateMain = True
        self.timeout = -1
        self.msg_update = ''
        self.ctrl.update_functions(self.updateMain, self.timeUpdateData)

    def updateMain(self, msg=''):
        self.fg_updateMain = True
        self.timeout = -1
        self.msg_update = msg

    def updateMQTT(self):

        print('updateMQTT: ')
        temp1, hmdt1, tempOut, hmdtOut = self.ctrl.getMeteo()
        # tempAir = self.ctrl.getTair()
        tempAir, tempWater = self.ctrl.getDStemp()
        tempBME, humBME, presBME = self.ctrl.getMeteoBME()
        co = self.ctrl.getCO()

        temp1 = None if temp1 == self.wrong_val else temp1
        hmdt1 = None if hmdt1 == self.wrong_val else hmdt1
        tempOut = None if tempOut == self.wrong_val else tempOut
        hmdtOut = None if hmdtOut == self.wrong_val else hmdtOut
        tempAir = None if tempAir == self.wrong_val else tempAir
        tempWater = None if tempWater == self.wrong_val else tempWater
        tempBME = None if tempBME == self.wrong_val else tempBME
        humBME = None if humBME == self.wrong_val else humBME
        presBME = None if presBME == self.wrong_val else presBME
        co = None if co == self.wrong_val else co

        # print('updateMQTT: ')
        # print("temp1: " + str(temp1))
        # print("hmdt1: " + str(hmdt1))
        # print("tempAir: " + str(tempAir))
        # print("tempWater: " + str(tempWater))
        # print("tempOut: " + str(tempOut))
        # print("hmdtOut: " + str(hmdtOut))
        # print("tempBME: " + str(tempBME))
        # print("humBME: " + str(humBME))
        # print("presBME: " + str(presBME))
        # print("co: " + str(co))

        # self.dbw.sendMeteo(temp1, hmdt1, tempAir)
        self.msg_update = '	тест2'
        ret = self.mqtt.sendMeteo(self.msg_update, temp1, hmdt1, tempAir, tempWater,
                            tempOut, hmdtOut, tempBME, humBME,
                            presBME, co)
        self.ctrl.set_sendStatus(ret)


    def start(self):
        self.ctrl.ledOff()
        self.internetTasks(0)
        self.ctrl.settingModel()
        #self.ctrl.startWeb()
        # self.ctrl.startBot()
        # self.display.clear()
        self.dispMeteo()
        self.ctrl.startCalcMeteo()
        self.ctrl.startWorkTM1637()
        self.ctrl.startworkRotary()
        print("work meteo.")
        self.ctrl.ledMainOn()
        fg_disp = 0
        start_time = time()
        self.ctrl.ledMainBlink(3)
        while True:

            if self.timeout < abs(time() - start_time):
                if self.fg_updateMain:
                    sleep(1)
                    res = self.internetTasks()
                    if res:
                        self.fg_updateMain = False
                    sleep(1)

                self.ctrl.ledGreenOff()

                self.dispMeteo(fg_disp)

                if fg_disp == 0:
                    fg_disp = 1
                    self.timeout = self.timeUpdateView
                    self.ctrl.ledGreenOn()
                elif fg_disp == 1:
                    fg_disp = 0
                    self.timeout = self.timeUpdateView/2
                start_time = time()
            else:
                sleep(1)


            # self.ctrl.ledMainBlink(0.3)
            # if i == 0:
            #     i += 1
            # #         ans = modem.send_sms3("+79276571758", str(temp))
            # #         ans = modem.send_sms3("+79171180617", str(temp))
            # #         print(ans)
            # break
            # self.ctrl.ledMainOn()
        self.ctrl.ledOff()
        self.ctrl.ledMainOff()

    def dispMeteo(self, fg=0):

        self.display.clear()
# fg == 0
        if fg == 0:
            temp, hum, tempOut, humOut = self.ctrl.getMeteo()
            # temp_air = self.ctrl.getTair()
            temp_air, temp_water = self.ctrl.getDStemp()
            # print('temp : ' + str(temp))
            # print('tempOut : ' + str(tempOut))
            # print('hum : ' + str(hum))
            # print('humOut : ' + str(humOut))

            # +++ First line
            line = 1
            txt11 = t_base_name + str(round(temp, 1))
            txt12 = '/' + str(int(hum))
            if temp_air != self.wrong_val:
                txt13 = t_air_name + str(round(temp_air, 1))
            else:
                txt13 = ''

            pos13 = len(txt11 + txt12) + 1
            if hum != self.wrong_val: pos13 += 1
            diff = self.display.max_col()-(pos13+len(txt13))
            if diff < 0: txt13 = txt13[:diff]

            self.display.text(txt11, line=line)
            self.display.text(txt12, line=line, pos=len(txt11))
            if hum != self.wrong_val:
                self.display.move_to(len(txt11 + txt12), 0)
                self.display.putchar(chr(1))
            self.display.text(txt13, line=line, pos=pos13)

            # +++ Second line
            line = 2
            txt21 = t_out_name + str(round(tempOut, 1))
            txt22 = '/' + str(int(humOut))
            if temp_air != self.wrong_val:
                txt23 = ' ' + t_water_name+str(round(temp_water,1))
            else:
                txt23 = ''

            pos23 = len(txt21 + txt22) + 0
            if humOut != self.wrong_val: pos23 += 1
            diff = self.display.max_col()-(pos23+len(txt23))
            if diff < 0: txt23 = txt23[:diff]

            self.display.text(txt21, line=line)
            self.display.move_to(len(txt21), 1)
            self.display.text(txt22, line=line, pos=len(txt21))
            if humOut != self.wrong_val:
                self.display.move_to(len(txt21 + txt22), 1)
                self.display.putchar(chr(1))
            self.display.text(txt23, line=line, pos=pos23)

# fg == 1
        if fg == 1:
            temp, hum, pres = self.ctrl.getMeteoBME()
            co = self.ctrl.getCO()
            # print('tempBME : ' + str(temp))
            # print('humBME : ' + str(hum))
            # print('presBME : ' + str(pres))
            # print('co : ' + str(co))

            # +++ First line
            line = 1
            txt11 = th_bme_name + str(round(temp, 1))
            txt12 = '/' + str(int(hum))
            txt13 = p_bme_name + str(round(pres, 0))

            pos13 = len(txt11 + txt12) + 2
            diff = self.display.max_col() - (pos13 + len(txt13))
            if diff < 0: txt13 = txt13[:diff]

            self.display.text(txt11, line=line)
            self.display.text(txt12, line=line, pos=len(txt11))
            self.display.move_to(len(txt11 + txt12), 0)
            self.display.putchar(chr(1))
            self.display.text(txt13, line=line, pos=pos13)

            # +++ Second line
            line = 2
            txt21 = co_name + str(round(co, 1))

            self.display.text(txt21, line=line)

    def internetTasks(self, fg=1):
        global thread_internet
        if not thread_internet:
            _thread.start_new_thread(self.internetTasksThread, (fg,))
            return True
        else:
            print("last internetTasks still running")
            return False

    def internetTasksThread(self, fg=1):
        global thread_internet
        # print('thread_internet' + str(thread_internet))
        thread_internet = True
        try:
            # print("Try internet...")
            attempt = 0
            while attempt < 3:
                stat = self.ctrl.internetStart()
                if stat:
                    sleep(1)
                    self.ctrl.updateRTCTime()
                    if fg == 1:
                        print("Internet update MQTT")
                        self.updateMQTT()
                    if fg == 0:
                        if self.fg1_ip_address:
                            self.ip_address = self.ctrl.getIpAddress()
                        if self.ip_address:
                            self.display.text('ip' + self.ip_address, line=1)
                            sleep(5)
                            self.fg1_ip_address = False
                    attempt = 3
                    sleep(1)
                    self.ctrl.internetStop()
                else:
                    attempt += 1
                    if attempt == 2:
                        self.ctrl.internetStop()
        except Exception as e:
            print(f"Error internetTasks: {e}")
        # print('thread_internet = False')
        thread_internet = False