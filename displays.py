# import ssd1306
import sh1106
# from i2c_lcd1602 import I2C_LCD1602
from lcd_api import LcdApi
from pico_i2c_lcd import I2cLcd
import utime
from pin_assign import pinAssign

address1106 = 0x3C
address1602 = 0x27

thermometer = bytearray([0x18, 0x18, 0x06, 0x09, 0x08, 0x08, 0x09, 0x06])
humidity = bytearray([0x04, 0x04, 0x0A, 0x0A, 0x11, 0x13, 0x17, 0x0E])
version = 'ver.14-02-2025\nuPython v1.24.1'

class Displays:

    def __init__(self):
        self.display1106 = None
        self.display1602 = None
        self.fg_display1106 = True
        self.fg_display1602 = True
        self.idx = None

    def display_1106(self):
        print('display_1106 adr ', address1106)
        self.idx = 0
        # ESP32 Pin assignment
        # i2c = I2C(-1, scl=Pin(22), sda=Pin(21), freq=400000)
        i2c = pinAssign().getDisplay(address1106)
        if i2c:
            oled_width = 128
            oled_height = 64
            self.display1106 = sh1106.SH1106_I2C(oled_width, oled_height, i2c, None, address1106)
            self.display1106.rotate(1)
            self.display1106.sleep(False)

            self.display1106.fill(0)
            self.display1106.text('HELLO!', 15, 30, 1)
            self.display1106.show()

        return self.display1106

    def display_1602(self):
        print("display_1602 adr ", address1602)
        self.idx = 1
        # self.display = I2C_LCD1602(i2c, adr)
        # self.display.puts("HELLO!")
        i2c = pinAssign().getDisplay(address1602)
        if i2c:
            self.display1602 = I2cLcd(i2c, address1602, 2, 16)
            self.display1602.clear()
            self.display1602.putstr(version)
            self.display1602.custom_char(0, thermometer)
            self.display1602.custom_char(1, humidity)
        return self.display1602

    def clear(self):
        if self.display1106 and self.fg_display1106:
            self.display1106.fill(0)
        if self.display1602 and self.fg_display1602:
            self.display1602.clear()

    def clear2(self):
        if self.display1106 and self.fg_display1106:
            self.display1106.fill(0)
    def text(self, txt, line=1, pos=0):
        if self.display1106 and self.fg_display1106:
            x = pos
            y = 0
            if line == 2:
                y = 10
            self.display1106.text(txt, x, y, 1)
            self.display1106.show()
        if self.display1602 and self.fg_display1602:
            self.display1602.move_to(pos, 0)
            if line == 2:
                self.display1602.move_to(pos, 1)
            self.display1602.putstr(txt)

    def custom_char(self, location, charmap):
        if self.display1106 and self.fg_display1106:
            pass
        if self.display1602 and self.fg_display1602:
            self.display1602.custom_char(location, charmap)

    def move_to(self, x, y):
        if self.display1106 and self.fg_display1106:
            pass
        if self.display1602 and self.fg_display1602:
            self.display1602.move_to(x, y)

    def putchar(self, char):
        if self.display1106 and self.fg_display1106:
            pass
        if self.display1602 and self.fg_display1602:
            self.display1602.putchar(char)

    def max_col(self, fg=1):
        if fg == 0:     #self.display1106:
            return 20
        if fg == 1:     #self.display1602:
            return 16



