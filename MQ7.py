# include "MQ7.h"
# Ported from https://github.com/amperka/TroykaMQ
# Author: Alexey Tveritinov [kartun@yandex.ru]
from machine import Pin, ADC
from BaseMQ import BaseMQ
from micropython import const

try:
    from pin_assign import pin_mq7
except:
    pin_mq7 = ''

# try:
#	from config import config
# except:
#	config = {}

pin_default = 34


class MQ7(BaseMQ):
    ## Clean air coefficient
    MQ7_RO_BASE = const(2)

    def __init__(self, pinData, pinHeater=-1, boardResistance=10000, baseVoltage=5,
                 measuringStrategy=BaseMQ.STRATEGY_ACCURATE):
        # Call superclass to fill attributes
        super().__init__(pinData, pinHeater, boardResistance, baseVoltage, measuringStrategy)

    # pass

    ## Measure Carbon monooxide
    def readCarbonMonoxide(self):
        return self.readScaled(-0.77, 3.38)

    ##  Base RO differs for every sensor family
    def getRoInCleanAir(self):
        return self.MQ7_RO_BASE


def sensorMQ7():
    if pin_mq7:
        pin = pin_mq7
    else:
        pin = pin_default
    if ADC(pin).read() == 0:
        return None
    return MQ7(pin)
