import time
import _thread
import neopixel
from pin_assign import pinLedMain

fg_led_blink = False

class ledMain:
    global fg_led_blink
    interval = 1
    def __init__(self):
        self.led = pinLedMain().get()
        self.interval_check = 0.1

    def on(self):
        global fg_led_blink
        fg_led_blink = False
        time.sleep(0.2)
        self.led.on()

    def off(self):
        global fg_led_blink
        fg_led_blink = False
        time.sleep(0.2)
        self.led.off()

    def blink(self, inteval=2):
        global fg_led_blink
        while fg_led_blink:
            fg_led_blink = False
            time.sleep(self.interval_check*1.2)
        self.interval = inteval
        if self.led:
            fg_led_blink = True
            _thread.start_new_thread(self.blink_thread, (self.interval,))

    def blink_thread(self, interval=1):
        def check(interval, interval_check):
            global fg_led_blink
            time_left = interval
            while time_left >= interval_check:
                time.sleep(interval_check)
                time_left -= interval_check
                if not fg_led_blink: return False
            if time_left > 0: time.sleep(time_left)
            if not fg_led_blink: return False
            return True
        i = 0
        print("Start blink")
        while True:
            self.led.on()
            if not check(interval, self.interval_check): break
            self.led.off()
            if not check(interval, self.interval_check): break
            i = i + 1
            # if (i % 10) == 0:
            #     print(i)
        print("End blink")

class ledRGB:
    interval = 1
    brightness_neopixel = 180
    def __init__(self):
        self.led_r = pinLedMain().get_red()
        self.led_g = pinLedMain().get_green()
        self.led_b = pinLedMain().get_blue()
        pin_neopixel = pinLedMain().get_neopixel()
        self.led_N = None
        if pin_neopixel:
            self.led_N = neopixel.NeoPixel(pin_neopixel, 1)
        self.interval_check = 0.1

    # def

    def on_red(self):
        if self.led_r: self.led_r.on()
        if self.led_N:
            self.led_N[0] = [self.brightness_neopixel, 0, 0]
            self.led_N.write()

    def off_red(self):
        if self.led_r: self.led_r.off()
        if self.led_N:
            self.led_N[0] = [0, 0, 0]
            self.led_N.write()

    def on_green(self):
        if self.led_g: self.led_g.on()
        if self.led_N:
            self.led_N[0] = [0, self.brightness_neopixel, 0]
            self.led_N.write()

    def off_green(self):
        if self.led_g: self.led_g.off()
        if self.led_N:
            self.led_N[0] = [0, 0, 0]
            self.led_N.write()

    def on_blue(self):
        if self.led_b: self.led_b.on()
        if self.led_N:
            self.led_N[0] = [0, 0, self.brightness_neopixel]
            self.led_N.write()

    def off_blue(self):
        if self.led_b: self.led_b.off()
        if self.led_N:
            self.led_N[0] = [0, 0, 0]
            self.led_N.write()

    def on_white(self):
        if self.led_N:
            self.led_N[0] = [self.brightness_neopixel]*3
            self.led_N.write()

    def off_white(self):
        if self.led_N:
            self.led_N[0] = [0, 0, 0]
            self.led_N.write()

    def off(self):
        if self.led_r: self.led_r.off()
        if self.led_g: self.led_g.off()
        if self.led_b: self.led_b.off()
        if self.led_N:
            self.led_N[0] = [0, 0, 0]
            self.led_N.write()

if __name__ == '__main__':
    ledRGB().on_white()