import time
from model1 import Model
from controller import Controller
from viewer import Viewer

print("1. Start model...")
model = Model()
print("2. Start controller...")
ctrl = Controller(model)
print("3. Start viewer...")
view = Viewer(ctrl)
time.sleep(3)
view.start()

# if __name__ == '__main__':
#     from time import sleep_ms
#     from machine import Pin
#
#     led=Pin(2,Pin.OUT) #create LED object from pin2,Set Pin2 to output
#     try:
#         while True:
#             led.value(1)        #Set led turn on
#             sleep_ms(1000)
#             led.value(0)        #Set led turn off
#             sleep_ms(1000)
#     except:
#         pass
#
#     from connect import internet
#     import mip
#     internet().start()
#     mip.install("github:mcauser/micropython-tm1637")