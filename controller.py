
import time
from leds import ledMain, ledRGB
from pin_assign import pinAssign, sensors_on
from connect import internet
from webpage import webpage1
# from telegrambot import botInfo

from MQ7 import sensorMQ7

class Controller():
    webpage = ''
    bot = None

    getmeteo_red = False
    getdstemp_red = False
    getmeteobme_red = False
    getco_red = False

    def __init__(self, inModel):
        self.model = inModel
        self.internet = internet()
        self.webpage = webpage1()
        self.led_main = ledMain()
        self.led_rgb = ledRGB()

    def settingModel(self):
        sensorMeteo = pinAssign().sensorMeteo1()
        sensorMeteoOut = pinAssign().sensorMeteoOut()
        self.model.setMeteo(sensorMeteo, sensorMeteoOut)
        # self.model.startMeteo()
        dev, roms = pinAssign().sensorDS()
        # self.model.setSensorTAir(dev, roms)
        self.model.setSensorDS(dev, roms)
        mq7 = sensorMQ7()
        self.model.setMQ7(mq7)
        tm1637 = pinAssign().getTM1637()
        self.model.setTM1637(tm1637)
        rotary = pinAssign().getRotary()
        self.model.setRotary(rotary)
        dev = pinAssign().getBme280()
        self.model.setBme280(dev)

    # def startBot(self):
    #     print('start Bot..')
    #     self.bot = botInfo()
    #     # self.bot.loop()

    def setWrongVal(self, val):
        self.model.setWrongVal(val)

    def startWeb(self):
        print('start webpage..')
        self.webpage.start()

    def ledRedOn(self):
        return self.led_rgb.on_red()

    def ledRedOff(self):
        return self.led_rgb.off_red()

    def ledGreenOn(self):
        return self.led_rgb.on_green()

    def ledGreenOff(self):
        return self.led_rgb.off_green()

    def ledBlueOn(self):
        return self.led_rgb.on_blue()

    def ledBlueOff(self):
        return self.led_rgb.off_blue()

    def ledOff(self):
        return self.led_rgb.off()
    def ledMainOn(self):
        return self.led_main.on()

    def ledMainOff(self):
        return self.led_main.off()

    def ledMainBlink(self, interval=1):
        return self.led_main.blink(interval)

    # def getDisplay1106(self):
    #     return pinAssign().getDisplay1106()
    # def getDisplay1602(self):
    #     return pinAssign().getDisplay1602()

    def internetStart(self):
        status = self.internet.start()
        if status:
            self.ledBlueOn()
        return status

    def internetStop(self):
        self.internet.stop()
        if not self.internet.ready:
            self.ledBlueOff()

    def getIpAddress(self):
        return self.internet.ip_address

    def update_functions(self, func, data):
        self.model.update_functions(func, data)

    def startCalcMeteo(self):
        self.model.startCalcMeteo()

    def getMeteo(self):
        val = self.model.getMeteo()
        if (self.model.wrong_val in val) and sensors_on.meteo and sensors_on.meteo_out:
            if not self.getmeteo_red:
                self.ledRedOn()
                self.getmeteo_red = True
        elif self.getmeteo_red:
            self.ledRedOff()
            self.getmeteo_red = False
        return val

    def getTair(self):
        val = self.model.getTair()
        # if val == self.model.wrong_val and sensors_on.air:
        #     self.ledRedOn()
        return val


    def getDStemp(self):
        val1, val2 = self.model.getDStemp()
        if (val1 == self.model.wrong_val and sensors_on.air) or (val2 == self.model.wrong_val and sensors_on.water):
            if not self.getdstemp_red:
                self.ledRedOn()
                self.getdstemp_red = True
        elif self.getdstemp_red:
            self.ledRedOff()
            self.getdstemp_red = False
        return val1, val2

    def getCO(self):
        val = self.model.getCO()
        if val == self.model.wrong_val and sensors_on.mq7:
            if not self.getco_red:
                self.ledRedOn()
                self.getco_red = True
        elif self.getco_red:
            self.ledRedOff()
            self.getco_red = False
        return val

    def getTM1637(self):
        return self.model.getTM1637()

    def startWorkTM1637(self):
        self.model.startWorkTM1637()

    def setRotary(self, r):
        self.model.setRotary(r)

    def startworkRotary(self):
        self.model.startworkRotary()

    def getMeteoBME(self):
        val = self.model.getMeteoBME()
        if (self.model.wrong_val in val) and sensors_on.bme280:
            if not self.getmeteobme_red:
                self.ledRedOn()
                self.getmeteobme_red = True
        elif self.getmeteobme_red:
            self.ledRedOff()
            self.getmeteobme_red = False
        return val

    def updateRTCTime(self):
        self.model.updateRTCTime()

    def getTemp(self):
        return self.model.sensorMeteo.temperature()

    def getHum(self):
        return self.model.sensorMeteo.humidity()

    def set_sendStatus(self, val):
        self.model.set_sendStatus(val)

    def updateWeb(self, temp, hum):
        if self.webpage:
            self.webpage.update(temp, hum)